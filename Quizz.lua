quizz_env = "PARTY";
--quizz_env = "GUILD";
-- quizz_env = "SAY";

NEXT_QUESTION_TIMING = 20;

name, realm = UnitFullName("player")
currentPlayer = name .. '-' .. realm

players = {}
players_total = {}
quizz_theme = '';
question_index = 1;

local QuizzFrame = CreateFrame("Frame");
QuizzFrame:RegisterEvent("CHAT_MSG_PARTY_LEADER");
QuizzFrame:RegisterEvent("CHAT_MSG_PARTY");
QuizzFrame:RegisterEvent("CHAT_MSG_SAY");
QuizzFrame:RegisterEvent("ADDON_LOADED");

debug = 0
new_party = 0

-- Handle answersq from chat
QuizzFrame:SetScript("OnEvent", function(self, event, message, sender)
    quizzAnswers(sender, message)
end)

-- Init game and send questions
SLASH_QUIZ1 = "/quiz"
SlashCmdList["QUIZ"] = function(msg)
    if msg == 'reset' then
        if new_party == 1 then
			SendChatMessage("Reset du quizz", quizz_env)
            resetQuizz()
        end
    elseif msg == 'total' then
        SendChatMessage("Les scores totaux sont :", quizz_env);
        local sortedPlayers = getKeysSortedByValue(players_total, function(a, b) return a > b end)
        for _, key in pairs(sortedPlayers) do
            SendChatMessage(key .. " " .. players_total[key] .. " points", quizz_env);
        end

        winner = getWinner()
        if winner['name'] ~= nil then
            SendChatMessage("Fin du quizz, le gagant est {rt1} " .. winner['name'] .. " {rt1} avec " .. winner['score'] .. " points.", quizz_env);
        else
            SendChatMessage(" Fin du quizz, pas de gagnants", quizz_env);
        end
    elseif msg == 'themes' then
        for id, name in pairs(themes) do
            SendChatMessage(" Theme " .. name .. " : " .. id, quizz_env);
        end
    elseif msg == 'debug' then
        if debug == 0 then
            debug = 1
            NEXT_QUESTION_TIMING = 40;
            print('debug on')
        else
            debug = 0
            NEXT_QUESTION_TIMING = 20;
            print('debug off')
        end
    else
--        name = msg:gsub("%_", " ")
        SendChatMessage("Thème du quizz est : " .. themes[msg], quizz_env)
        new_party = 1;
        quizz_theme = msg;
        sendQuestion()
        timer = 0
        Ctimer = C_Timer.NewTicker(1, function() skipQuestion() end)
    end
end

function quizzAnswers(sender, message)
    answer_key = 'a' .. question_index;
    answer = string.lower(quizz_questions[quizz_theme][answer_key])
    message = string.lower(message)

    -- Clear special char from player message
    message = message:gsub("%[", "")
    message = message:gsub("%]", "")
--    message = message:gsub("le ", "")
--    message = message:gsub("la ", "")
--    message = message:gsub("l'", "")

    -- Check if Julien Lepers is not answering his own questions
    if ((sender ~= currentPlayer and debug == 0) or debug == 1) and new_party == 1 then
        -- Message contains answer
        -- Create an answer without special char for illiterate
        cleanedAnswer = cleanSpecialChar(answer)
        if string.find(message, answer) ~= nil or string.find(message, cleanedAnswer) ~= nil then
            if players[sender] ~= nil then
                players[sender] = players[sender] + 1;
            else
                players[sender] = 1;
            end

            SendChatMessage("{rt5} Bonne réponse ! C'est bien " .. quizz_questions[quizz_theme][answer_key] .. '. <' .. sender .. "> est à " .. players[sender] .. " points.", quizz_env);

            -- If the last question is answered, display scoreboard
            checkNextQuestion()
        else
            -- Check if the player message contains part of the answer, if true, send a whisp to help him
            pattern = "%S+"
            has_answer_part = {}
            i = 0
            answer_parts = '';
            for word in string.gmatch(message, pattern) do
                if string.find(answer, word) ~= nil then
                    has_answer_part[i] = word
                    i = i + 1
                end
            end
            if getTableLength(has_answer_part) >= 1 then
                for k, v in pairs(has_answer_part) do
                    ans = v:gsub("%...", "")
                    if string.len(ans) >= 3 then
                        if answer_parts == '' then
                            answer_parts = ans
                        else
                            answer_parts = answer_parts .. ', ' .. ans
                        end
                    end
                end
                if string.len(answer_parts) > 0 then
                    SendChatMessage("La réponse contient bien " .. answer_parts, "WHISPER", nil, sender);
                end
            end
        end
    end
end

function cleanSpecialChar(text)
    text = text:gsub("%é", "e")
    text = text:gsub("%è", "e")
    text = text:gsub("%à", "a")
    text = text:gsub("%-", " ")
    text = text:gsub("%ç", "c")
    text = text:gsub("%ù", "u")
    return text
end

function sendQuestion()
    if new_party == 1 then
--        theme_questions = Questions:getQuestions(quizz_theme, index)
        question_key = 'q' .. question_index;
        SendChatMessage("{rt4} Question " .. question_index .. " : " .. quizz_questions[quizz_theme][question_key], quizz_env);
    end
end

function jumpToNextQuestion()
    question_index = question_index + 1
    timer = 0
    sendQuestion()
end

function skipQuestion()
--    SendChatMessage("Timer question actuelle index " .. question_index, quizz_env);
    if new_party == 1 then
        timer = timer + 1
        answer = quizz_questions[quizz_theme][answer_key];
        if timer == (NEXT_QUESTION_TIMING / 2) then
            SendChatMessage("{rt3} Indice, " .. string.sub(answer, 1, 1) .. "...", quizz_env);
        end
        if timer == NEXT_QUESTION_TIMING then
            SendChatMessage("{rt7} Personne n'a trouvé. La réponse était " .. answer, quizz_env);
            checkNextQuestion();
        end
    end
end

function checkNextQuestion()
    if question_index == (getTableLength(quizz_questions[quizz_theme]) / 2) then
        local sortedPlayers = getKeysSortedByValue(players, function(a, b) return a > b end)
        if getTableLength(sortedPlayers) > 0 then
            SendChatMessage("Les scores sont :", quizz_env);
            for _, key in pairs(sortedPlayers) do
                SendChatMessage(key .. " " .. players[key] .. " points", quizz_env);
                -- Add score to total players array
--                    if players_total[key] ~= nil then
--                        players_total[key] = players_total[key] + players[key]
--                    else
--                        players_total[key] = players[key]
--                    end
            end
        else
            SendChatMessage("{rt8} Aucun gagnant {rt8}", quizz_env);
        end
        resetQuizz()
    else
        jumpToNextQuestion()
    end
end

function resetQuizz()
    Ctimer:Cancel();
    players = {}
    question_index = 1;
    new_party = 0;
end

function getWinner()
	winner_name = nil;
	winner_score = 0;
	exaequo = 0;
    winners = {}
	for player_name, player_score in pairs(players) do
		if winner_name == nil then
			winner_name = player_name;
			winner_score = player_score;
		elseif player_score == winner_name then
			exaequo = 1;
			winner_name = nil;
			winner_score = 0;
		else
			if player_score > winner_score then
				winner_score = player_score
				winner_name = player_name;
				exaequo = 0;
			end
		end
	end

    winner = {}
    winner['score'] = winner_score
    winner['name'] = winner_name
	return winner
end

function getTableLength(tbl)
  local count = 0
  for k, v in pairs(tbl) do
	  if v ~= nil then
	  	count = count + 1
	  end
  end
  return count
end

function getKeysSortedByValue(tbl, sortFunction)
  local keys = {}
  for key, value in pairs(tbl) do
    table.insert(keys, key)
  end

  table.sort(keys, function(a, b)
    return sortFunction(tbl[a], tbl[b])
  end)

  return keys
end